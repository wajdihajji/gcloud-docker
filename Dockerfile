FROM docker:stable-git

# Set gcloud and kubectl versions
ENV GCLOUD_VERSION "396.0.0"
ENV KUBECTL_VERSION "1.22"

# Ensure our scripts and gcloud SDK are on the PATH
ENV PATH=/usr/local/bin:/usr/bin:/sbin:/bin:/usr/local/gcloud/google-cloud-sdk/bin

# Install gcloud dependencies
RUN apk add python3 curl && apk upgrade

COPY ./install.sh /tmp/
RUN /tmp/install.sh && rm /tmp/install.sh
COPY ./bin/ /usr/local/bin/
