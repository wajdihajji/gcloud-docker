# gcloud and Docker image

This image is the based `docker:stable-git` image with:

* The latest GCloud SDK `gcloud` command line tool installed.
* The beta components for `gcloud` installed.
* Google `cloud_sql_proxy` installed.
* `kubectl` and [authentication plugin](https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-access-for-kubectl#install_plugin)
installed with `gcloud`.

Example `.gitlab-ci.yml` usage:

```yaml
some_deploy_job:
  image: some-registry/gcloud-docker:latest
  services:
    - docker:dind

  script:
    # Configure GCloud with the service account credentials and project.
    # Requires GOOGLE_CI_ACCOUNT_CREDENTIALS and GOOGLE_PROJECT be defined.
    - setup_gcloud

    # Start the Google cloud proxy and wait a short period for it to start
    - cloud_sql_proxy "-instances=${CLOUD_SQL_INSTANCE}" -dir cloudsql & sleep 5

    # ... actual deployment goes here ...

  only:
    variables:
      - "${GOOGLE_CI_ACCOUNT_CREDENTIALS}"
      - "${GOOGLE_PROJECT}"
      - "${CLOUD_SQL_INSTANCE}"
```
