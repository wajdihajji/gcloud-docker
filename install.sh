#!/usr/bin/env sh

# Install GCloud SDK
SDK_TARBALL=$(mktemp -t google-cloud-sdk.tar.gz.XXXXXX)

if test -z "$GCLOUD_VERSION" || [ "$GCLOUD_VERSION" = "latest" ];
then
    echo "Installing latest gcloud release"
    curl https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz >"${SDK_TARBALL}"
else
    echo "Installing gcloud version ${GCLOUD_VERSION}"
    curl "https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${GCLOUD_VERSION}-linux-x86_64.tar.gz" \
        >"${SDK_TARBALL}"
fi

mkdir -p /usr/local/gcloud
tar -C /usr/local/gcloud -xvf "${SDK_TARBALL}"
rm "${SDK_TARBALL}"

/usr/local/gcloud/google-cloud-sdk/install.sh || exit 1

# Ensure gcloud command is on the PATH
PATH=${PATH}:/usr/local/gcloud/google-cloud-sdk/bin

gcloud components install beta

# Install k8s kubectl and authentication plugin
gcloud components install kubectl gke-gcloud-auth-plugin

# Use the specifed kubectl version or fall back to the latest version
if [ -n "$KUBECTL_VERSION" ]; then
    KUBECTL_BIN=/usr/local/gcloud/google-cloud-sdk/bin/kubectl
    KUBECTL_VERSION_DIR=$(dirname $KUBECTL_BIN)/kubectl.$KUBECTL_VERSION
    if [ -e $KUBECTL_VERSION_DIR ]; then
        rm $KUBECTL_BIN
        ln -s $KUBECTL_VERSION_DIR $KUBECTL_BIN
    fi
fi

# Clean up backup and __pycache__ to reduce image size
rm -rf /usr/local/gcloud/google-cloud-sdk/.install/.backup
rm -rf $(find /usr/local/gcloud/google-cloud-sdk/ -regex ".*/__pycache__") \

# Install the Cloud SQL proxy. The proxy will use the same credentials as
# the gcloud CLI tool.
curl https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 >/usr/local/bin/cloud_sql_proxy
chmod +x /usr/local/bin/cloud_sql_proxy
